import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:travel_book/blocs/my_places_cubit.dart';
import 'package:travel_book/repositories/my_places_repository.dart';
import 'package:travel_book/ui/pages/travelogue_page.dart';
import 'package:travel_book/ui/widgets/logo_and_text.dart';
import 'package:travel_book/ui/widgets/places_list/my_places_list.dart';

void main() {
  late Widget travelogueWidget;
  setUp(() {
    final MyPlacesRepository myPlacesRepository = MyPlacesRepository();
    travelogueWidget = BlocProvider<MyPlacesCubit>(
      create: (context) => MyPlacesCubit(myPlacesRepository),
      child: const MaterialApp(
        home: TraveloguePage(),
      ),
    );
  });

  group('Travelogue Page - Elements - Tests', () {
    testWidgets('Test Tab Bar should be present', (tester) async {
      await tester.pumpWidget(travelogueWidget);
      expect(find.byType(TabBar), findsOneWidget);
    });

    testWidgets('Test Tab Bar should have 4 Tab', (tester) async {
      await tester.pumpWidget(travelogueWidget);
      expect(find.byType(Tab), findsNWidgets(4));
      expect(find.text("Tous"), findsOneWidget);
      expect(find.text("Favoris"), findsOneWidget);
      expect(find.text("Visités"), findsOneWidget);
      expect(find.text("ToDo"), findsOneWidget);
    });

    testWidgets('MyPlaceslist should be present', (tester) async {
      await tester.pumpWidget(travelogueWidget);
      expect(find.byType(MyPlacesList), findsOneWidget);
    });

    testWidgets('Logo and Text should be present', (tester) async {
      await tester.pumpWidget(travelogueWidget);
      expect(find.byType(LogoAndText), findsOneWidget);
    });
  });
}