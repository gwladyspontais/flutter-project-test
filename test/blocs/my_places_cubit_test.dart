import 'package:flutter_test/flutter_test.dart';
import 'package:travel_book/blocs/my_places_cubit.dart';
import 'package:travel_book/models/place.dart';
import 'package:travel_book/repositories/my_places_repository.dart';

void main() {
  setUp(() => TestWidgetsFlutterBinding.ensureInitialized());

  test('A new place should be added and remove', () {
    final repo = MyPlacesRepository();
    final cubit = MyPlacesCubit(repo);

    Place place = Place(
        id: '1',
        name: 'test',
        adresse: 'test',
        imagePath: 'test',
        nbRate: 5,
        rate: 5,
        city: 'test');

    cubit.addMyPlace(place);
    expect(cubit.state[0].place, place);
    cubit.removeMyPlace('1');
    expect(cubit.state.isEmpty, true);
  });
}
