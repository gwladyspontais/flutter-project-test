# Travel book

## Description

Le projet consiste à développer une application "Travel book" qui permettra aux utilisateurs de recentrer les lieux qu'ils ont ou ceux qu'ils comptent visités. Les fonctionnalités principales de l'application incluent la visualisation des lieux visités par l'utilisateur, la possibilité de rechercher des lieux ainsi que la consultation de ceux près de lui.

### Descriptif des fonctionnalités :

1. Création de compte : Les utilisateurs peuvent s'enregistrer et se connecter par la suite.

2. Rechercher des lieux près de soi : L'application offre la possibilité de consulter les lieux aux alentour de soi si l'application à l'autorisation pour la localisation et si la localisation est activée sur le téléphone de l'utilisateur.

3. Recherche de lieux : Les utilisateurs peuvent effectuer des recherches pour trouver de nouveaux lieux à visiter.

4. Sauvegarde de lieux : Les utilisateurs peuvent ajouter un lieu à leur carnet. Ils peuvent également les classés par favoris, visités ou todo.

5. Visualisation du carnet : L'application fournira une fonctionnalité pour visualiser les endroits que l'utilisateur a visités ou qu'il comptent les visités, permettant ainsi de revivre les expériences passées.

L'objectif global du projet est de fournir une application pratique pour permettre aux utilisateurs de garder une trace de leurs voyages et de découvrir de nouveaux endroits intéressants.

## Installer le projet

### Étape 1 :
Téléchargez ou clonez ce dépôt.

### Étape 2 :
Exécutez la commande suivante dans la console pour obtenir les dépendances requises :
```flutter pub get```

### Étape 3 :
Lancer le projet !

## Informations techniques

### Version flutter et dart
Pour démarrer le projet il vous faut la version de flutter ```3.10.3```.
Concernant Dart, il vous faur la version ```Dart 3.0.3```.

### API utilisée et outils externes
Le projet n'utilise que l'API de google place et firebase.

Nous avons utilisé ces routes :

1. Exemple pour récupérer toutes les infos
```https://maps.googleapis.com/maps/api/place/details/json?fields=name,photo,rating,formatted_address&place_id=ChIJN1t_tDeuEmsRUsoyG83frY4&key=API_KEY```

2. Exemple récupérer les infos via recherche
```https://maps.googleapis.com/maps/api/place/textsearch/json?input=parc&inputtype=textquery&type=tourist_attraction&language=fr&key=API_KEY```

3. Exemple récupérer données avec lat et long
```https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=48.858370,2.294481&radius=1500&key=API_KEY```

4. Exemple page suivantes
```https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=PAGETOKEN&key=API_KEY```
