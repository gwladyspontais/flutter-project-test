import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/ui/themes/custom_colors.dart';

import '../../blocs/profile_cubit.dart';
import '../../blocs/profile_state.dart';

class LogoAndText extends StatelessWidget {
  final String? title;
  final String? text;

  const LogoAndText(
      {Key? key, this.title, this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Opacity(
          opacity: 0.5,
          child: BlocBuilder<ProfileCubit, ProfileState>(
            builder: (context, state) {
              if (state.profile?.darkMode == true) {
                return Image.asset(
                  'assets/logo/Logo_dark.png',
                  width: double.infinity,
                );
              }
              return Image.asset(
                'assets/logo/Logo.png',
                width: double.infinity,
              );
            }
          ),
        ),
        Container(padding: const EdgeInsets.all(20),child: Column(children: [
          if (title != null)
          Text(
            title ?? "",
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: darkOrange, fontSize: 30),
          ),
          Container(height: 20),
          if (text != null)
          Text(text ?? "",
              style: Theme.of(context).textTheme.displayMedium, textAlign: TextAlign.justify),
        ]),)

      ],
    );
  }
}
