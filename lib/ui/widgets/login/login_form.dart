import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/ui/themes/custom_colors.dart';
import 'package:travel_book/ui/widgets/alert_dialog/alert_dialog_reset_password.dart';

import '../../../blocs/user_cubit.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    final UserCubit userCubit = context.read<UserCubit>();

    final GlobalKey<FormState> loginFormKey = GlobalKey();
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    return Container(
      margin: const EdgeInsets.all(10),
      child: Form(
        key: loginFormKey,
        child: ListView(
          children: [
            TextFormField(
              controller: emailController,
              decoration: const InputDecoration(
                labelText: 'E-mail *',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Le champs doit être renseigné';
                } else {
                  return null;
                }
              },
            ),
            TextFormField(
              obscureText: true,
              controller: passwordController,
              decoration: const InputDecoration(
                labelText: 'Mot de passe *',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Le champs doit être renseigné';
                } else {
                  return null;
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "Mot de passe oublié ? Réinitialisez-le en entrant votre adresse e-mail ci-dessus puis cliquez sur le bouton ci-dessous.",
              textAlign: TextAlign.justify,
              style: Theme.of(context).textTheme.displayMedium
            ),
            TextButton(
                onPressed: () async {
                  var value = emailController.text;
                  if (value == null || value.isEmpty) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      backgroundColor: error,
                      content:
                          const Text("Veuillez entrer une adresse e-mail."),
                      action: SnackBarAction(
                        label: 'Fermer',
                        textColor: Colors.white,
                        onPressed: () {},
                      ),
                    ));
                  } else {
                    showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialogResetPassword(email: value)
                    );
                  }
                },
                child: const Text('Mot de passe oublié')),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () async {
                  if (loginFormKey.currentState!.validate()) {
                    final String email = emailController.text;
                    final String password = passwordController.text;
                    userCubit.login(email, password);
                  }
                },
                child: const Text('Valider')),
          ],
        ),
      ),
    );
  }
}
