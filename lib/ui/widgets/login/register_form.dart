import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/user_cubit.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  @override
  Widget build(BuildContext context) {
    final UserCubit userCubit = context.read<UserCubit>();

    final GlobalKey<FormState> registerFormKey = GlobalKey();
    final TextEditingController emailController = TextEditingController();
    final TextEditingController pseudoController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();
    final TextEditingController confirmPasswordController = TextEditingController();

    return Container(
      margin: const EdgeInsets.all(10),
      child: Form(
        key: registerFormKey,
        child: ListView(
          children: [
            TextFormField(
              controller: emailController,
              decoration: const InputDecoration(
                labelText: 'E-mail *',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Le champs doit être renseigné';
                } else {
                  return null;
                }
              },
            ),
            TextFormField(
              controller: pseudoController,
              decoration: const InputDecoration(
                labelText: 'Pseudo *',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Le champs doit être renseigné';
                } else {
                  return null;
                }
              },
            ),
            TextFormField(
              obscureText: true,
              controller: passwordController,
              decoration: const InputDecoration(
                labelText: 'Mot de passe *',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Le champs doit être renseigné';
                } else {
                  return null;
                }
              },
            ),
            TextFormField(
              obscureText: true,
              controller: confirmPasswordController,
              decoration: const InputDecoration(
                labelText: 'Confirmer votre Mot de passe *',
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Le champs doit être renseigné';
                }
                if (value != passwordController.text) {
                  return 'Les mots de passe ne correspondent pas';
                }
              },
            ),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () async {
                  if (registerFormKey.currentState!.validate()) {
                    final String email = emailController.text;
                    final String pseudo = pseudoController.text;
                    final String password = passwordController.text;
                    userCubit.register(email, pseudo, password);
                  }
                },
                child: const Text('Valider')
            ),
          ],
        ),
      ),
    );
  }
}
