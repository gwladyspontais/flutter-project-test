import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/ui/widgets/alert_dialog/alert_dialog_rgpd.dart';

import '../../../blocs/user_cubit.dart';
import '../../themes/custom_colors.dart';
import '../alert_dialog/alert_dialog_delete_user.dart';

class Account extends StatefulWidget {
  const Account({Key? key}) : super(key: key);

  @override
  State<Account> createState() => _AccountState();
}

class _AccountState extends State<Account> {
  get userCubit => null;

  @override
  Widget build(BuildContext context) {
    final UserCubit userCubit = context.read<UserCubit>();

    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(children: [
        SizedBox(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "E-mail",
                  style: Theme.of(context).textTheme.displaySmall,
                ),
                Text(
                  userCubit.userRepository.firebaseAuth.currentUser?.email ??
                      "",
                  style: Theme.of(context).textTheme.displayLarge,
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(children: [
                  Text("RGPD", style: Theme.of(context).textTheme.displaySmall,),
                  IconButton(
                      icon: const Icon(Icons.info_outline_rounded),
                      color: darkBlue,
                      onPressed: () => showDialog<String>(
                        context: context,
                        builder: (BuildContext context) =>
                        const AlertDialogRGPD()),
                  ),
                ],)
              ],
            )),
        const SizedBox(
          height: 20,
        ),
        TextButton(
            onPressed: () async {
              //TODO
            },
            child: const Text("Modifier mon e-mail")),
        const SizedBox(
          height: 20,
        ),
        TextButton(
            onPressed: () async {
              //TODO
            },
            child: const Text("Changer de mot de passe")),
        const SizedBox(
          height: 20,
        ),
        TextButton(
            onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) =>
                    const AlertDialogDeleteUser()),
            child: const Text("Supprimer son compte")),
        const Spacer(),
        ElevatedButton(
            onPressed: () async {
              userCubit.logout();
            },
            child: const Text("Se déconnecter")),
      ]),
    );
  }
}
