import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/models/data_state.dart';
import '../../../blocs/profile_cubit.dart';
import '../../../blocs/profile_state.dart';
import '../../themes/custom_colors.dart';

class UserInfo extends StatefulWidget {
  const UserInfo({Key? key}) : super(key: key);

  @override
  State<UserInfo> createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  bool _isDarkMode = false;

  @override
  Widget build(BuildContext context) {
    final ProfileCubit profileCubit = context.read<ProfileCubit>();
    profileCubit.initProfile();

    return Container(
        padding: const EdgeInsets.all(20),
        child: BlocBuilder<ProfileCubit, ProfileState>(builder: (context, state) {
          if (state.dataState == DataState.loaded) {
            _isDarkMode = state.profile?.darkMode ?? false;
          }
          return Column(children: [
            ListTile(
              leading: const Icon(Icons.person),
              title: Text(state.profile?.pseudo ?? "Pseudo"),
              trailing: IconButton(
                  icon: const Icon(Icons.edit),
                  color: darkBlue,
                  onPressed: () {
                    //TODO;
                  }
              ),
            ),
            ListTile(
              leading: const Icon(Icons.location_city_rounded),
              title: Text(state.profile?.city ?? "City"),
              trailing: IconButton(
                  icon: const Icon(Icons.edit),
                  color: darkBlue,
                  onPressed: () {
                    //TODO;
                  }
              ),
            ),
            ListTile(
              leading: const Icon(Icons.location_on),
              title: const Text('Localisation'),
              trailing: IconButton(
                  icon: const Icon(Icons.edit),
                  color: darkBlue,
                  onPressed: () {
                    profileCubit.changeLocalisationPermission();
                  }
              ),
            ),
            SwitchListTile(
              title: const Text('Mode nuit'),
              value: _isDarkMode,
              onChanged: (bool value) {
                setState(() {
                  _isDarkMode = value;
                  profileCubit.userRepository.setDarkMode(value);
                });
              },
              secondary: const Icon(Icons.dark_mode),
            ),
            const Spacer(),
            ElevatedButton(
                onPressed: () async {
                  //TODO
                },
                child: const Text("Sauvegarder mon carnet de voyage")),
          ]);
        })
    );
  }
}
