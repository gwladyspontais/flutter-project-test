import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/my_places_cubit.dart';
import '../../themes/custom_colors.dart';

class AlertDialogRemoveMyPlace extends StatelessWidget {
  final String placeId;
  const AlertDialogRemoveMyPlace({super.key, required this.placeId});

  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();

    return AlertDialog(
      icon: const Icon(Icons.warning_amber_rounded, color: darkOrange, size: 60),
      title: const Text('Retirer du carnet', style: TextStyle(color: darkOrange, fontWeight: FontWeight.bold)),
      content: const Text('Cette action retire ce lieu de votre carnet, listes Favoris, Visités et Todo incluses.', textAlign: TextAlign.justify),
      actions: <Widget>[
        TextButton(
          onPressed: () => {
            myPlacesCubit.removeMyPlace(placeId),
            Navigator.pop(context)
          },
          child: const Text('Confirmer'),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Annuler'),
        ),
      ],
    );
  }
}
