import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/user_cubit.dart';
import '../../themes/custom_colors.dart';

class AlertDialogResetPassword extends StatefulWidget {
  final String email;

  const AlertDialogResetPassword({super.key, required this.email});

  @override
  State<AlertDialogResetPassword> createState() => _AlertDialogResetPasswordState();
}

class _AlertDialogResetPasswordState extends State<AlertDialogResetPassword> {
  @override
  Widget build(BuildContext context) {
    final UserCubit userCubit = context.read<UserCubit>();

    return AlertDialog(
      icon: const Icon(Icons.warning_amber_rounded, color: darkOrange, size: 60),
      title: const Text('Réinitialisation du mot de passe',
          style: TextStyle(color: darkOrange, fontWeight: FontWeight.bold)),
      content: Text(
          "Un e-mail pour réinitialiser votre mot de passe sera envoyé à l'adresse suivante. Veuillez vérifier qu'elle soit valide.\n${widget.email}",
          textAlign: TextAlign.justify),
      actions: <Widget>[
        TextButton(
          onPressed: () => {
            userCubit.resetPassword(widget.email),
            Navigator.pop(context)
          },
          child: const Text('Confirmer'),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Annuler'),
        ),
      ],
    );
  }
}
