import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/user_cubit.dart';
import '../../themes/custom_colors.dart';

class AlertDialogRGPD extends StatelessWidget {
  const AlertDialogRGPD({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final UserCubit userCubit = context.read<UserCubit>();

    return AlertDialog(
      icon: const Icon(Icons.info_outline_rounded, color: darkOrange, size: 60),
      title: const Text('Données personnelles', style: TextStyle(color: darkOrange, fontWeight: FontWeight.bold)),
      content: const Text('Liste des données personnelles enregistrées et utilisation : \nE-mail : authentification. \nPseudo : identification. \nLocalisation : uniquement utilisée pour la recherche de lieux aux alentours.\n\nLes lieux de votre carnet sont uniquement enregistés sur votre téléphone.\n\nVous pouvez supprimer toutes vos données en supprimant votre compte.', textAlign: TextAlign.justify),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Fermer'),
        ),
      ],
    );
  }
}
