import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/user_cubit.dart';
import '../../themes/custom_colors.dart';

class AlertDialogDeleteUser extends StatelessWidget {
  const AlertDialogDeleteUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final UserCubit userCubit = context.read<UserCubit>();

    return AlertDialog(
      icon: const Icon(Icons.warning_amber_rounded, color: darkOrange, size: 60),
      title: const Text('Suppression du compte', style: TextStyle(color: darkOrange, fontWeight: FontWeight.bold)),
      content: const Text('Attention ! Cette action est irreversible. En supprimant votre compte vous supprimez toutes les données associées.', textAlign: TextAlign.justify),
      actions: <Widget>[
        TextButton(
          onPressed: () => {
            userCubit.deleteUser(),
            Navigator.pop(context)
          },
          child: const Text('Confirmer'),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: const Text('Annuler'),
        ),
      ],
    );
  }
}
