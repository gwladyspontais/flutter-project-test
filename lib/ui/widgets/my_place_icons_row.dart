import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/models/place.dart';
import 'package:travel_book/ui/themes/custom_colors.dart';

import '../../blocs/my_places_cubit.dart';
import '../../models/my_place.dart';

class MyPlaceIconsRow extends StatefulWidget {
  final Place place;

  const MyPlaceIconsRow({super.key, required this.place});

  @override
  State<MyPlaceIconsRow> createState() => _MyPlaceIconsRowState();
}

class _MyPlaceIconsRowState extends State<MyPlaceIconsRow> {
  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();
    myPlacesCubit.loadMyPlacesList();
    return BlocBuilder<MyPlacesCubit, List<MyPlace>>(
        builder: (BuildContext context, List<MyPlace> myPlacesList) {
          var isInMyPlaces = myPlacesCubit.isInMyPlaces(widget.place.id);
          var isFavorite = isInMyPlaces && myPlacesCubit.isFavorite(widget.place.id);
          var isVisited = isInMyPlaces && myPlacesCubit.isVisited(widget.place.id);
          var isTodo = isInMyPlaces && myPlacesCubit.isTodo(widget.place.id);

          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Spacer(),
            if (isFavorite)
              IconButton(
                  icon: const Icon(Icons.favorite),
                  color: darkOrange,
                  onPressed: () {
                    myPlacesCubit.removeFavorite(widget.place.id);
                  }
              )
            else
              IconButton(
                  icon: const Icon(Icons.favorite_outline),
                  color: darkBlue,
                  onPressed: () {
                    myPlacesCubit.addFavorite(widget.place);
                  }
              ),
            const Text("Favoris", style: TextStyle(color: darkBlue)),
            const Spacer(),
            if (isVisited)
              IconButton(
                  icon: const Icon(Icons.check_circle_rounded),
                  color: darkOrange,
                  onPressed: () {
                    myPlacesCubit.removeStatus(widget.place.id);
                  }
              )
            else
              IconButton(
                  icon: const Icon(Icons.check_circle_outline_rounded),
                  color: darkBlue,
                  onPressed: () {
                    myPlacesCubit.addVisited(widget.place);
                  }
              ),
                const Text("Visité", style: TextStyle(color: darkBlue)),
                const Spacer(),
            if (isTodo)
              IconButton(
                  icon: const Icon(Icons.check_box_rounded),
                  color: darkOrange,
                  onPressed: () {
                    myPlacesCubit.removeStatus(widget.place.id);
                  }
              )
            else
              IconButton(
                  icon: const Icon(Icons.check_box_outlined),
                  color: darkBlue,
                  onPressed: () {
                    myPlacesCubit.addTodo(widget.place);
                  }
              ),
                const Text("ToDo", style: TextStyle(color: darkBlue)),
                const Spacer(),
          ]);
    });
  }
}
