import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/models/list_type.dart';

import '../../../blocs/my_places_cubit.dart';
import '../../../models/my_place.dart';
import '../../../models/place.dart';
import '../../pages/place_page.dart';
import '../../themes/custom_colors.dart';
import '../alert_dialog/alert_dialogue_remove_my_place.dart';
import '../rate.dart';

class PlaceCard extends StatefulWidget {
  final Place place;
  final ListType listType;

  const PlaceCard({super.key, required this.place, required this.listType});

  @override
  State<PlaceCard> createState() => _PlaceCardState();
}

class _PlaceCardState extends State<PlaceCard> {
  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();
    myPlacesCubit.loadMyPlacesList();
    var place = widget.place;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(place.name,
            style: Theme.of(context).textTheme.bodyLarge),
        ListTile(
          contentPadding: const EdgeInsets.all(5.0),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(place.city),
              Rate(place: place),
            ],
          ),
          leading: SizedBox(
            height: 100.0,
            width: 100.0,
            child: Image.network(
              "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=AIzaSyDWRTWkImZ1rC7AFiABDhjMqsY9twxWDPE&photo_reference=${place.imagePath}",
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return Image.asset(
                  'assets/logo/Logo_text.png',
                  width: double.infinity,
                );
              },
            ),
          ),
          trailing: Builder(builder: (context) {
            switch (widget.listType) {
              case ListType.todo:
                return IconButton(
                    icon: const Icon(Icons.check_circle_outline_rounded),
                    color: blue,
                    onPressed: () {
                      myPlacesCubit.addVisited(widget.place);
                    });
              case ListType.search:
                return BlocBuilder<MyPlacesCubit, List<MyPlace>>(builder:
                    (BuildContext context, List<MyPlace> myPlacesList) {
                  if (myPlacesCubit.isInMyPlaces(widget.place.id)) {
                    return IconButton(
                        icon: const Icon(Icons.menu_book_rounded),
                        color: darkOrange,
                        onPressed: () {
                          if (myPlacesCubit.isFavorite(widget.place.id) ||
                              myPlacesCubit.isVisited(widget.place.id) ||
                              myPlacesCubit.isTodo(widget.place.id)) {
                            showDialog<String>(
                                context: context,
                                builder: (BuildContext context) =>
                                    AlertDialogRemoveMyPlace(
                                        placeId: widget.place.id));
                          } else {
                            myPlacesCubit.removeMyPlace(widget.place.id);
                          }
                        });
                  } else {
                    return IconButton(
                        icon: const Icon(Icons.menu_book_rounded),
                        color: blue,
                        onPressed: () {
                          myPlacesCubit.addMyPlace(widget.place);
                        });
                  }
                });
              default:
                return Container(width: 0);
            }
          }),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PlacePage(place: place),
            ));
          },
        ),
      ],
    );
  }
}
