import 'package:flutter/material.dart';
import 'package:travel_book/models/list_type.dart';
import 'package:travel_book/ui/widgets/places_list/place_card.dart';
import '../../../models/place.dart';
import '../logo_and_text.dart';

class ListPlaces extends StatelessWidget {
  final List<Place> places;
  final String? titleEmpty;
  final String? textEmpty;

  const ListPlaces({Key? key, required this.places, this.titleEmpty, this.textEmpty}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (places.isEmpty) {
      return LogoAndText(title: titleEmpty, text: textEmpty);
    }
    return ListView.builder(
      itemCount: places.length,
      itemBuilder: (context, index) {
        final place = places[index];
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: PlaceCard(place: place, listType: ListType.search)
        );
      },
    );
  }
}
