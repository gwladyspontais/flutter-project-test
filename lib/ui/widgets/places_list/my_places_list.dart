import 'package:flutter/cupertino.dart';
import 'package:travel_book/models/list_type.dart';
import 'package:travel_book/ui/widgets/places_list/place_card.dart';

import '../../../models/my_place.dart';
import '../logo_and_text.dart';

class MyPlacesList extends StatefulWidget {
  final List<MyPlace> myPlaces;
  final ListType listType;
  final String? titleEmpty;
  final String? textEmpty;

  const MyPlacesList({super.key, required this.myPlaces, required this.listType, this.titleEmpty, this.textEmpty});

  @override
  State<MyPlacesList> createState() => _MyPlacesListState();
}

class _MyPlacesListState extends State<MyPlacesList> {
  @override
  Widget build(BuildContext context) {
    var myPlaces = widget.myPlaces;
    if (myPlaces.isEmpty) {
      return LogoAndText(title: widget.titleEmpty, text: widget.textEmpty);
    }
    return ListView.builder(
      itemCount: myPlaces.length,
      itemBuilder: (context, index) {
        final myPlace = myPlaces[index];
        return Padding(
            padding: const EdgeInsets.all(8.0),
            child: PlaceCard(place: myPlace.place, listType: widget.listType)
        );
      },
    );
  }
}
