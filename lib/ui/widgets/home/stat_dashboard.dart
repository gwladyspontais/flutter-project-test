import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../blocs/my_places_cubit.dart';

class StatDashboard extends StatelessWidget {
  final String stat;
  final Color color;
  final String title;

  const StatDashboard({Key? key, required this.stat, required this.color, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();
    myPlacesCubit.loadMyPlacesList();

    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            stat,
            style: TextStyle(
              color: color,
              fontSize: 24,
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(title),
        ],
      ),
    );
  }
}
