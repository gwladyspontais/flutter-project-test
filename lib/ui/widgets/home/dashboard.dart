import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/ui/widgets/home/stat_dashboard.dart';

import '../../../blocs/my_places_cubit.dart';
import '../../../models/my_place.dart';
import '../../themes/custom_colors.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();
    myPlacesCubit.loadMyPlacesList();

    return Container(
      width: 300,
      height: 100,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: Colors.grey.withOpacity(0.5)),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            blurRadius: 5,
            offset: const Offset(0, 0),
          ),
        ],
      ),
      child: BlocBuilder<MyPlacesCubit, List<MyPlace>>(
          builder: (BuildContext context, List<MyPlace> myPlacesList) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            StatDashboard(stat: myPlacesCubit.getFavoriteNumber().toString(), color: darkOrange, title: 'Favoris'),
            StatDashboard(stat: myPlacesCubit.getVisitedNumber().toString(), color: darkBlue, title: 'Visités'),
            StatDashboard(stat: myPlacesCubit.getTodoNumber().toString(), color: lightBlue, title: 'Todo'),
          ],
        );
      }),
    );
  }
}
