import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:travel_book/ui/themes/custom_colors.dart';
import '../../models/place.dart';

class Rate extends StatelessWidget {
  final Place place;

  const Rate({Key? key, required this.place}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text((place.rate).toString()),
            RatingBar.builder(
              ignoreGestures: true,
              initialRating: place.rate.toDouble(),
              minRating: 1,
              itemSize: 20.0,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemBuilder: (context, _) => const Icon(
                Icons.star,
                color: lightOrange,
              ),
              onRatingUpdate: (rating) {
              },
            ),
          ],
        ),
        Text(
            "(${place.nbRate.toString()} votes)",
            style: const TextStyle(fontSize: 12.0)
        ),
      ],
    );
  }
}


