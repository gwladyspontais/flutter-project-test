import 'dart:ui';

const darkBlue = Color(0xFF336B87);
const blue = Color(0xFF6C93A4);
const lightBlue = Color(0xFFA1BDCA);
const veryLightBlue = Color(0xFFD8ECF0);

const lightOrange = Color(0xFFFCD19C);
const orange = Color(0xFFDEA14E);
const darkOrange = Color(0xFFD68959);

const error = Color(0xFFB1384E);

const backgroundColor = Color(0xFFF8F9FA);
const darkBackground = Color(0xFF1F2933);

const grey = Color(0xFF83909D);
const white = Color(0xFFFFFFFF);
const black = Color(0xFF000000);