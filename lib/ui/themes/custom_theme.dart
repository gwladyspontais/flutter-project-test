import 'package:flutter/material.dart';

import 'custom_colors.dart';

class CustomTheme {
  static ThemeData get lightTheme {
    return ThemeData(
      colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: darkBlue,
          onPrimary: white,
          secondary: darkOrange,
          onSecondary: white,
          error: error,
          onError: darkBlue,
          background: backgroundColor,
          onBackground: black,
          surface: backgroundColor,
          onSurface: black),
      scaffoldBackgroundColor: backgroundColor,
      appBarTheme: const AppBarTheme(
          backgroundColor: backgroundColor,
          foregroundColor: darkBlue,
          titleTextStyle: TextStyle(
              fontWeight: FontWeight.bold, color: darkBlue, fontSize: 32),
          elevation: 0),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
            padding: MaterialStateProperty.all(const EdgeInsets.all(12)),
            textStyle: MaterialStateProperty.all(
                const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
            textStyle: MaterialStateProperty.all(
                const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))),
      ),
      tabBarTheme: const TabBarTheme(
          labelColor: darkOrange,
          labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          unselectedLabelColor: lightBlue,
          unselectedLabelStyle:
              TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          overlayColor: MaterialStatePropertyAll(Colors.transparent)),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        selectedItemColor: darkOrange,
        unselectedItemColor: blue,
      ),
      switchTheme: SwitchThemeData(
          thumbColor: MaterialStateProperty.resolveWith((states) =>
              states.contains(MaterialState.selected) ? darkOrange : darkBlue),
          trackColor: MaterialStateProperty.resolveWith((states) =>
              states.contains(MaterialState.selected)
                  ? lightOrange
                  : lightBlue)),
      listTileTheme: const ListTileThemeData(iconColor: blue),
      textTheme: const TextTheme(
        bodySmall: TextStyle(
            color: darkBlue, fontWeight: FontWeight.bold, fontSize: 20),
        displayMedium: TextStyle(color: black, fontSize: 15),
        displayLarge: TextStyle(color: black, fontSize: 14, height: 2),
        displaySmall: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 14, height: 2, color: black),
        bodyLarge: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: darkBlue),
      ),
      iconTheme: const IconThemeData(
        color: darkBlue, // Customize the icon color here
      ),
    );
  }

  static ThemeData get darkTheme {
    return ThemeData(
      brightness: Brightness.dark,
      colorScheme: const ColorScheme(
        brightness: Brightness.dark,
        primary: darkOrange,
        onPrimary: white,
        secondary: darkBlue,
        onSecondary: white,
        error: error,
        onError: white,
        background: darkBackground,
        onBackground: white,
        surface: darkBackground,
        onSurface: white,
      ),
      scaffoldBackgroundColor: darkBackground,
      appBarTheme: const AppBarTheme(
        backgroundColor: darkBackground,
        foregroundColor: white,
        titleTextStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: white,
          fontSize: 32,
        ),
        elevation: 0,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.all(12)),
          textStyle: MaterialStateProperty.all(
            const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: white,
            ),
          ),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          textStyle: MaterialStateProperty.all(
            const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: white,
            ),
          ),
        ),
      ),
      tabBarTheme: const TabBarTheme(
        labelColor: darkOrange,
        labelStyle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
        unselectedLabelColor: grey,
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
        overlayColor: MaterialStatePropertyAll(Colors.transparent),
      ),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        selectedItemColor: orange,
        unselectedItemColor: grey,
      ),
      switchTheme: SwitchThemeData(
        thumbColor: MaterialStateProperty.resolveWith((states) =>
            states.contains(MaterialState.selected) ? darkOrange : darkBlue),
        trackColor: MaterialStateProperty.resolveWith((states) =>
            states.contains(MaterialState.selected) ? lightOrange : lightBlue),
      ),
      listTileTheme: const ListTileThemeData(
        iconColor: white,
      ),
      textTheme: const TextTheme(
        bodyMedium: TextStyle(
          color: black,
        ),
        displayLarge: TextStyle(color: white, fontSize: 14, height: 2),
        displayMedium: TextStyle(color: white, fontSize: 15),
        displaySmall: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 14, height: 2, color: white),
        bodySmall: TextStyle(
            color: lightOrange, fontWeight: FontWeight.bold, fontSize: 20),
        bodyLarge: TextStyle(
            fontSize: 16, fontWeight: FontWeight.bold, color: lightBlue),
      ),
      iconTheme: const IconThemeData(
        color: lightOrange, // Customize the icon color here
      ),
    );
  }
}
