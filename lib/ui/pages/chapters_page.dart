import 'package:flutter/material.dart';
import '../widgets/logo_and_text.dart';

class ChaptersPage extends StatefulWidget {
  const ChaptersPage({Key? key}) : super(key: key);

  @override
  State<ChaptersPage> createState() => _ChaptersPageState();
}

class _ChaptersPageState extends State<ChaptersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chapitres"),
      ),
      body: const LogoAndText(
          title: "Fonctionnalité à venir",
          text: "Cette fonctionnalité n'est pas encore implémentée. Elle sera disponible lors de la prochaine mise à jour.\nVous voulez en savoir plus ? Les chapitres permettront de regrouper les lieux que vous enregistrez dans votre carnet."
      ),
    );
  }
}
