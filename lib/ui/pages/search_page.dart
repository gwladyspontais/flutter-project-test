import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/ui/widgets/places_list/list_places.dart';

import '../../blocs/places_cubit.dart';
import '../../blocs/places_state.dart';
import '../../models/data_state.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key, this.menuScreenContext}) : super(key: key);

  final BuildContext? menuScreenContext;

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _searchController = TextEditingController();

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final PlacesCubit placesCubit = context.read<PlacesCubit>();
    placesCubit.loadPlaces(false, '');

    return Scaffold(
      appBar: AppBar(
        title: const Text("Recherche"),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _searchController,
              decoration: const InputDecoration(
                hintText: "Rechercher...",
                prefixIcon: Icon(Icons.search),
              ),
              onChanged: (value) {
                context.read<PlacesCubit>().loadPlaces(false, value);
              },
            ),
          ),
          Expanded(
            child: BlocBuilder<PlacesCubit, PlacesState>(
                builder: (context, state) {
              switch (state.dataState) {
                case DataState.loading:
                  return SizedBox(
                      height: MediaQuery.of(context).size.height,
                      child: const Center(child: CircularProgressIndicator()));
                case DataState.loaded:
                  return ListPlaces(
                    places: state.listPlaces,
                    titleEmpty: 'Aucun résultat',
                    textEmpty:
                        "Veuillez relancer votre recherche avec d'autres critères.",
                  );
                case DataState.error:
                  return const Center(
                    child:
                        Text('Une erreur est survenue, veuillez recommencer'),
                  );
              }
            }),
          ),
        ],
      ),
    );
  }
}
