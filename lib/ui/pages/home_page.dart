import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/ui/themes/custom_colors.dart';
import 'package:travel_book/ui/widgets/home/dashboard.dart';
import '../../blocs/places_cubit.dart';
import '../../blocs/places_state.dart';
import '../../models/data_state.dart';
import '../widgets/places_list/list_places.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, this.menuScreenContext}) : super(key: key);

  final BuildContext? menuScreenContext;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    final PlacesCubit placesCubit = context.read<PlacesCubit>();
    placesCubit.loadPlaces(true, '');

    return Scaffold(
      appBar: AppBar(
        title: const Text("Bienvenue"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          children: [
            const Dashboard(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Lieux près de moi",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                IconButton(
                  icon: const Icon(Icons.refresh),
                  onPressed: () {
                    placesCubit.loadPlaces(true, '');
                  },
                ),
              ],
            ),
            Expanded(
              child: BlocBuilder<PlacesCubit, PlacesState>(
                  builder: (context, state) {
                switch (state.dataState) {
                  case DataState.loading:
                    return SizedBox(
                        height: MediaQuery.of(context).size.height,
                        child:
                            const Center(child: CircularProgressIndicator()));
                  case DataState.loaded:
                    return ListPlaces(
                      places: state.listPlaces,
                      titleEmpty: 'Veuillez actualiser',
                    );
                  case DataState.error:
                    return const Center(
                      child:
                          Text('Une erreur est survenue, veuillez recommencer'),
                    );
                }
              }),
            ),
          ],
        ),
      ),
    );
  }
}
