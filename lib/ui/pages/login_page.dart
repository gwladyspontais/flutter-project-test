import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:travel_book/ui/widgets/login/login_form.dart';
import 'package:travel_book/ui/widgets/login/register_form.dart';
import '../../blocs/user_cubit.dart';
import '../../blocs/user_state.dart';
import '../../utils/auth_exception_handler.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    bool isKeyboardVisible =
        KeyboardVisibilityProvider.isKeyboardVisible(context);

    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            toolbarHeight: 10,
          ),
          body: Center(
            child: BlocListener(
              bloc: BlocProvider.of<UserCubit>(context),
              listener: (BuildContext context, UserState state) {
                if (state.error != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      backgroundColor: Colors.red,
                      content: Text(AuthExceptionHandler.generateErrorMessage(state.error)),
                      action: SnackBarAction(
                        label: 'Fermer',
                        textColor: Colors.white,
                        onPressed: () {},
                      ),
                    ),
                  );
                }
              },
              child: Column(
                children: [
                  Visibility(
                      visible: isKeyboardVisible ? false : true,
                      child: Image.asset(
                        'assets/logo/Logo_image.png',
                        width: double.infinity,
                      )),
                  Image.asset(
                    'assets/logo/Logo_text.png',
                    width: double.infinity,
                  ),
                  const TabBar(
                      tabs: [
                        Tab(child: Text("Connexion")),
                        Tab(child: Text("Inscription")),
                      ]),
                  const Expanded(
                    child: TabBarView(children: [
                      LoginForm(),
                      RegisterForm(),
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
