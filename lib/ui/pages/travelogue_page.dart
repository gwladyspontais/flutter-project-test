import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/models/list_type.dart';
import 'package:travel_book/ui/widgets/places_list/my_places_list.dart';

import '../../blocs/my_places_cubit.dart';
import '../../models/my_place.dart';

class TraveloguePage extends StatefulWidget {
  const TraveloguePage({Key? key}) : super(key: key);

  @override
  State<TraveloguePage> createState() => _TraveloguePageState();
}

class _TraveloguePageState extends State<TraveloguePage> {
  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();
    myPlacesCubit.loadMyPlacesList();

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Carnet de voyage"),
        ),
        body: BlocBuilder<MyPlacesCubit, List<MyPlace>>(
            builder: (BuildContext context, List<MyPlace> myPlacesList) {
          return Column(
            children: [
              const TabBar(
                isScrollable: true,
                tabs: [
                  Tab(child: Text("Tous")),
                  Tab(child: Text("Favoris")),
                  Tab(child: Text("Visités")),
                  Tab(child: Text("ToDo")),
                ]
              ),
              Expanded(
                child: TabBarView(children: [
                  MyPlacesList(
                    myPlaces: myPlacesList,
                    listType: ListType.none,
                    titleEmpty: 'Aucun lieu ajouté',
                    textEmpty: "Vous n'avez pas encore ajouté de lieux à votre carnet de voyage ! Rendez-vous sur la page de recherche pour enregistrer un lieu.\nCes lieux seront enregistés dans le stockage de votre téléphone.",
                  ),
                  MyPlacesList(
                    myPlaces: myPlacesCubit.getFavoritePlaces(),
                    listType: ListType.none,
                    titleEmpty: 'Aucun favoris',
                    textEmpty: "Vous n'avez pas encore ajouté de lieux favoris à votre carnet de voyage ! Rendez-vous sur la page d'un lieu et cliquez sur l'icône 'favoris'.",
                  ),
                  MyPlacesList(
                    myPlaces:
                        myPlacesCubit.getPlacesByStatus(MyPlaceStatus.visited),
                    listType: ListType.none,
                    titleEmpty: "Aucun lieu déjà visité",
                    textEmpty: "Vous n'avez pas encore ajouté de lieux déjà visité à votre carnet de voyage ! Rendez-vous sur la page d'un lieu et cliquez sur l'icône 'Visité'.",
                  ),
                  MyPlacesList(
                    myPlaces:
                        myPlacesCubit.getPlacesByStatus(MyPlaceStatus.todo),
                    listType: ListType.todo,
                    titleEmpty: "Aucun lieu à visiter",
                    textEmpty: "Vous n'avez pas encore ajouté de lieux à visiter à votre carnet de voyage ! Rendez-vous sur la page d'un lieu et cliquez sur l'icône 'ToDo'.",
                  ),
                ]),
              ),
            ],
          );
        }),
      ),
    );
  }
}
