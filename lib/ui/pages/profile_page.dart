import 'package:flutter/material.dart';
import 'package:travel_book/ui/widgets/profile/account.dart';
import 'package:travel_book/ui/widgets/profile/user_info.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Profil"),
        ),
        body: const Column(
          children: [
            TabBar(
                isScrollable: true,
                tabs: [
                  Tab(child: Text("Mes informations")),
                  Tab(child: Text("Mon compte")),
                ]
            ),
            Expanded(
              child: TabBarView(children: [
                UserInfo(),
                Account()
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
