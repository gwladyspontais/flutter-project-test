import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/repositories/places_repository.dart';
import 'package:travel_book/ui/widgets/alert_dialog/alert_dialogue_remove_my_place.dart';
import 'package:travel_book/ui/widgets/my_place_icons_row.dart';
import '../../blocs/my_places_cubit.dart';
import '../../models/my_place.dart';
import '../../models/place.dart';
import '../widgets/rate.dart';

class PlacePage extends StatefulWidget {
  final Place place;

  const PlacePage({Key? key, required this.place}) : super(key: key);

  @override
  State<PlacePage> createState() => _PlacePageState();
}

class _PlacePageState extends State<PlacePage> {
  List<String> photoReferences = [];
  PlacesRepository placeRepository = PlacesRepository();
  String? selectedPhotoReference;

  @override
  void initState() {
    super.initState();
    fetchPhotos();
  }

  Future<void> fetchPhotos() async {
    try {
      final List<String> photos =
          await placeRepository.getPhotoPlace(widget.place.id);
      setState(() {
        photoReferences = photos;
        selectedPhotoReference = photos.isNotEmpty ? photos[0] : '';
      });
    } catch (e) {return;}
  }

  @override
  Widget build(BuildContext context) {
    final MyPlacesCubit myPlacesCubit = context.read<MyPlacesCubit>();
    myPlacesCubit.loadMyPlacesList();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.place.name),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 300,
            child: Image.network(
              "https://maps.googleapis.com/maps/api/place/photo?maxheight=400&key=AIzaSyDWRTWkImZ1rC7AFiABDhjMqsY9twxWDPE&photo_reference=$selectedPhotoReference",
              errorBuilder: (context, error, stackTrace) {
                return Image.asset(
                  'assets/logo/Logo.png',
                  width: double.infinity,
                );
              },
            ),
          ),
          SizedBox(
            height: 100,
            child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              children: photoReferences.map((photoReference) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedPhotoReference = photoReference;
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(
                      "https://maps.googleapis.com/maps/api/place/photo?maxheight=400&key=AIzaSyDWRTWkImZ1rC7AFiABDhjMqsY9twxWDPE&photo_reference=$photoReference",
                      fit: BoxFit.cover,
                      errorBuilder: (context, error, stackTrace) {
                        return Image.asset(
                          'assets/logo/Logo.png',
                          width: double.infinity,
                        );
                      },
                    ),
                  ),
                );
              }).toList(),
            ),
          ),
          MyPlaceIconsRow(place: widget.place),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              "Emplacement",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              widget.place.adresse,
              style: const TextStyle(fontSize: 16),
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              "Avis",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Rate(place: widget.place),
              ],
            ),
          ),
          Center(
            child: BlocBuilder<MyPlacesCubit, List<MyPlace>>(
                builder: (BuildContext context, List<MyPlace> myPlacesList) {
              var isInMyPlaces = myPlacesCubit.isInMyPlaces(widget.place.id);
              if (isInMyPlaces) {
                return ElevatedButton(
                    onPressed: () {
                      if (myPlacesCubit.isFavorite(widget.place.id) || myPlacesCubit.isVisited(widget.place.id) || myPlacesCubit.isTodo(widget.place.id)) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) =>
                            AlertDialogRemoveMyPlace(placeId: widget.place.id));
                      } else {
                        myPlacesCubit.removeMyPlace(widget.place.id);
                      }
                    },
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.book),
                        SizedBox(
                          width: 20,
                        ),
                        Text("Retirer du carnet"),
                      ],
                    ));
              } else {
                return ElevatedButton(
                    onPressed: () {
                      myPlacesCubit.addMyPlace(widget.place);
                    },
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.book),
                        SizedBox(
                          width: 20,
                        ),
                        Text("Ajouter au carnet"),
                      ],
                    ));
              }
            }),
          ),
        ],
      ),
    );
  }
}
