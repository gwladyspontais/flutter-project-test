import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/blocs/user_state.dart';

import '../repositories/user_repository.dart';
import '../utils/auth_exception_handler.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit(this.userRepository) : super(UserState.deconnected());

  final UserRepository userRepository;

  Future<void> init() async {
    emit(UserState.loading());
    try {
      if (await userRepository.init()) {
        emit(UserState.connected());
      } else {
        emit(UserState.deconnected());
      }
    } on FirebaseAuthException catch (e) {
      handleFirebaseAuthError(e);
    }
  }

  Future<void> login(String email, String password) async {
    emit(UserState.loading());
    try {
      await userRepository.login(email, password);
      emit(UserState.connected());
    } on FirebaseAuthException catch (e) {
      if (e.code == "user-not-found") {
        handleFirebaseAuthError(FirebaseAuthException(code: "wrong-password"));
      } else {
        handleFirebaseAuthError(e);
      }
    }
  }

  Future<void> register(String email, String pseudo, String password) async {
    emit(UserState.loading());
    try {
      await userRepository.register(email, pseudo, password);
      emit(UserState.connected());
    } on FirebaseAuthException catch (e) {
      handleFirebaseAuthError(e);
    }
  }

  Future<void> resetPassword(String email) async {
    try {
      await userRepository.resetPassword(email);
    } on FirebaseAuthException catch (e) {
      handleFirebaseAuthError(e);
    }
  }

  Future<void> logout() async {
    try {
      await userRepository.logout();
      emit(UserState.deconnected());
    } on FirebaseAuthException catch (e) {
      handleFirebaseAuthError(e);
    }
  }

  Future<void> deleteUser() async {
    try {
      await userRepository.deleteUser();
      emit(UserState.deconnected());
    } on FirebaseAuthException catch (e) {
      handleFirebaseAuthError(e);
    }
  }

  void handleFirebaseAuthError(FirebaseAuthException e) {
    log(e.toString());
    AuthError? error = AuthExceptionHandler.handleAuthException(e);
    if (error != null) {
      emit(UserState.error(AuthExceptionHandler.handleAuthException(e)));
    }
  }
}