import '../models/data_state.dart';
import '../utils/auth_exception_handler.dart';

class UserState {
  AuthStatus status;
  AuthError? error;

  UserState(this.status, this.error);

  factory UserState.loading() => UserState(AuthStatus.loading, null);

  factory UserState.connected() => UserState(AuthStatus.connected, null);

  factory UserState.deconnected() => UserState(AuthStatus.deconnected, null);

  factory UserState.error(AuthError? error) => UserState(AuthStatus.error, error);
}