import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:travel_book/repositories/chapters_repository.dart';

import '../models/chapter.dart';

class ChaptersCubit extends Cubit<List<Chapter>> {

  ChaptersCubit(this.chaptersRepository) : super([]);

  final ChaptersRepository chaptersRepository;

  // TODO
}