import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:travel_book/blocs/profile_state.dart';
import '../models/profile.dart';
import '../repositories/user_repository.dart';

class ProfileCubit extends Cubit<ProfileState> {
  final UserRepository userRepository;

  late Position currentPosition = const Position(
    latitude: 0.0,
    longitude: 0.0,
    timestamp: null,
    accuracy: 0.0,
    altitude: 0.0,
    heading: 0.0,
    speed: 0.0,
    speedAccuracy: 0.0,
  );

  ProfileCubit(this.userRepository) : super(ProfileState.loading());

  Future<void> initProfile() async {
    emit(ProfileState.loading());
    try {
      emit(ProfileState.loaded(await userRepository.getProfile()));
    } catch (e) {
      emit(ProfileState.error());
    }
  }

  Future<void> updateProfile(Profile profile) async {
    emit(ProfileState.loading());
    try {
      await userRepository.updateProfile(profile);
      emit(ProfileState.loaded(profile));
    } catch (e) {
      emit(ProfileState.error());
    }
  }

  Future<void> changeLocalisationPermission() async {
    await Geolocator.openAppSettings();
  }

  Future<void> localisationPermitted() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await Geolocator.openLocationSettings();
    }

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    userRepository.setLocalisationPermitted(serviceEnabled);

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
  }

  Future<void> getCurrentLocalisation() async {
    await localisationPermitted();
    try {
      Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.bestForNavigation
      );
      currentPosition = position;
    } catch (e) {
      return;
    }
  }
}
