import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../models/my_place.dart';
import '../models/place.dart';
import '../repositories/my_places_repository.dart';

class MyPlacesCubit extends Cubit<List<MyPlace>> {
  final MyPlacesRepository myPlacesRepository;
  MyPlacesCubit(this.myPlacesRepository) : super([]);

  Future<void> loadMyPlacesList() async {
    emit(await myPlacesRepository.loadMyPlacesList());
  }

  bool isInMyPlaces(String placeId) {
    return state.where((myPlace) => myPlace.place.id == placeId).isNotEmpty ? true : false;
  }

  MyPlace getPlace(String placeId) {
    return state.firstWhere((myPlace) => myPlace.place.id == placeId);
  }

  bool isFavorite(String placeId) {
    return getPlace(placeId).favorite ? true : false;
  }

  bool isVisited(String placeId) {
    return getPlace(placeId).myPlaceStatus == MyPlaceStatus.visited ? true : false;
  }

  bool isTodo(String placeId) {
    return getPlace(placeId).myPlaceStatus == MyPlaceStatus.todo ? true : false;
  }

  Future<void> saveMyPlacesList(state) async {
    myPlacesRepository.saveMyPlaces(state);
    loadMyPlacesList();
  }

  Future<void> addMyPlace(Place place) async {
    if (isInMyPlaces(place.id)) {
      log("Place ${place.id} already in my places");
    } else {
      state.add(MyPlace(place, MyPlaceStatus.none, false));
      emit([...state]);
      saveMyPlacesList(state);
    }
  }

  Future<void> removeMyPlace(String placeId) async {
    state.removeWhere((myPlace) => myPlace.place.id == placeId);
    emit([...state]);
    saveMyPlacesList(state);
  }

  Future<void> addFavorite(Place place) async {
    if (isInMyPlaces(place.id)) {
      state.firstWhere((myPlace) => myPlace.place.id == place.id).favorite = true;
    } else {
      state.add(MyPlace(place, MyPlaceStatus.none, true));
      emit([...state]);
    }
    saveMyPlacesList(state);
  }

  Future<void> removeFavorite(String placeId) async {
    state.firstWhere((myPlace) => myPlace.place.id == placeId).favorite = false;
    emit([...state]);
    saveMyPlacesList(state);
  }

  Future<void> changeStatus(String placeId, MyPlaceStatus newStatus) async {
    state.firstWhere((myPlace) => myPlace.place.id == placeId).myPlaceStatus = newStatus;
    emit([...state]);
    saveMyPlacesList(state);
  }

  Future<void> addVisited(Place place) async {
    if (isInMyPlaces(place.id)) {
      changeStatus(place.id, MyPlaceStatus.visited);
    } else {
      state.add(MyPlace(place, MyPlaceStatus.visited, false));
      emit([...state]);
      saveMyPlacesList(state);
    }
  }

  Future<void> removeStatus(String placeId) async {
    changeStatus(placeId, MyPlaceStatus.none);
  }

  Future<void> addTodo(Place place) async {
    if (isInMyPlaces(place.id)) {
      changeStatus(place.id, MyPlaceStatus.todo);
    } else {
      state.add(MyPlace(place, MyPlaceStatus.todo, false));
      emit([...state]);
      saveMyPlacesList(state);
    }
  }

  int getMyPlacesNumber() {
    return state.length;
  }

  int getFavoriteNumber() {
    return getFavoritePlaces().length;
  }

  List<MyPlace> getFavoritePlaces() {
    return state.where((myPlace) => myPlace.favorite).toList();
  }

  int getVisitedNumber() {
    return getPlacesByStatus(MyPlaceStatus.visited).length;
  }

  List<MyPlace> getPlacesByStatus(MyPlaceStatus status) {
    return state.where((myPlace) => myPlace.myPlaceStatus == status).toList();
  }

  int getTodoNumber() {
    return getPlacesByStatus(MyPlaceStatus.todo).length;
  }

}