import '../models/data_state.dart';
import '../models/place.dart';

class PlacesState {
  DataState dataState;
  List<Place> listPlaces;

  PlacesState(this.dataState, [this.listPlaces = const []]);

  factory PlacesState.loading() => PlacesState(DataState.loading);

  factory PlacesState.loaded(List<Place> listPlaces) =>
      PlacesState(DataState.loaded, listPlaces);

  factory PlacesState.error() => PlacesState(DataState.error);
}
