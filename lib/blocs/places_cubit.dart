import 'dart:developer';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:travel_book/blocs/places_state.dart';
import 'package:travel_book/blocs/profile_cubit.dart';
import '../models/place.dart';
import '../repositories/places_repository.dart';

class PlacesCubit extends Cubit<PlacesState> {
  PlacesCubit(this.placesRepository, this.profileCubit) : super(PlacesState.loading());

  final PlacesRepository placesRepository;
  final ProfileCubit profileCubit;

  Future<void> loadPlaces(bool isHomePage, String query) async {
    try {
      emit(PlacesState.loading());
      if (isHomePage) {
        Position currentPosition;
        String latitude;
        String longitude;

        profileCubit.getCurrentLocalisation();
        currentPosition = profileCubit.currentPosition;
        latitude = currentPosition.latitude.toString();
        longitude = currentPosition.longitude.toString();
        final userPlaces = await placesRepository.fetchPlacesNearUser(latitude, longitude);
        emit(PlacesState.loaded(userPlaces));
      } else {
        List<Place> allPlaces =
            await placesRepository.fetchPlacesNearEiffelTower();
        final searchPlaces =
            await placesRepository.filterPlaces(allPlaces, query);
        emit(PlacesState.loaded(searchPlaces));
      }
    } catch (e) {
      log(e.toString());
      emit(PlacesState.error());
    }
  }
}
