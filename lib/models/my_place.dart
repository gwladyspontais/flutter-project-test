import 'package:travel_book/models/place.dart';

class MyPlace{
  final Place place;
  MyPlaceStatus myPlaceStatus;
  bool favorite;
  num? myRate;

  MyPlace(
    this.place,
    this.myPlaceStatus,
    this.favorite,
    [this.myRate]
  );

  Map<String, dynamic> toJson() => {'place': place.toJson(), 'myPlaceStatus': myPlaceStatus.toStringValue(), 'favorite': favorite, 'myRate': myRate};
  factory MyPlace.fromJson(Map<String, dynamic> json) => MyPlace(Place.fromJsonFromStorage(json['place']), parseEnum(json['myPlaceStatus']), json['favorite'], json['myRate']);
}

enum MyPlaceStatus {
  visited,
  todo,
  none
}

extension MyPlaceStatusExtension on MyPlaceStatus {
  String toStringValue() {
    switch (this) {
      case MyPlaceStatus.visited:
        return 'visited';
      case MyPlaceStatus.todo:
        return 'todo';
      case MyPlaceStatus.none:
        return 'none';
      default:
        throw ArgumentError('Valeur d\'énumération non valide: $this');
    }
  }
}

MyPlaceStatus parseEnum(String value) {
  switch (value) {
    case 'visited':
      return MyPlaceStatus.visited;
    case 'todo':
      return MyPlaceStatus.todo;
    case 'none':
      return MyPlaceStatus.none;
    default:
      throw ArgumentError('Valeur d\'énumération non valide: $value');
  }
}