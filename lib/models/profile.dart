class Profile {
  final String pseudo;
  final String? city;
  final bool darkMode;

  Profile(this.pseudo, this.darkMode, [this.city]);

  Map<String, dynamic> toJson() => {'pseudo': pseudo, 'darkMode': darkMode, 'city': city,};
  factory Profile.fromJson(Map<String, dynamic> json) => Profile(json['pseudo'], json['darkMode'], json['city'],);
}