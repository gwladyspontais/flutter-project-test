enum DataState {
  loading,
  loaded,
  error
}

enum AuthStatus {
  loading,
  connected,
  deconnected,
  error
}