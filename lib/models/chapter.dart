class Chapter {
  final String title;
  final List<int> placesList;

  Chapter(this.title, this.placesList);

  Map<String, dynamic> toJson() => {'title': title, 'placesList': placesList};
  factory Chapter.fromJson(Map<String, dynamic> json) => Chapter(json['title'], json['placesList']);
}