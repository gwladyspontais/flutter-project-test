class Place {
  final String id;
  final String name;
  final String imagePath;
  final String adresse;
  final num rate;
  final num nbRate;
  final String city;

  Place({
    required this.id,
    required this.name,
    required this.imagePath,
    required this.adresse,
    required this.rate,
    required this.nbRate,
    required this.city,
  });

  factory Place.fromJson(Map<String, dynamic> json) {
    final photos = json['photos'] as List<dynamic>?;
    final photoReference = photos != null && photos.isNotEmpty
        ? photos[0]['photo_reference'] ?? ''
        : '';

    final adresse = json['vicinity'] ?? '';
    final city = getCity(adresse);

    return Place(
      id: json['place_id'] ?? '',
      name: json['name'] ?? '',
      imagePath: photoReference,
      adresse: adresse,
      rate: json['rating'] ?? 0.0,
      nbRate: json['user_ratings_total'] ?? 0.0,
      city: city,
    );
  }

  factory Place.fromJsonSearch(Map<String, dynamic> json) {
    final photos = json['photos'] as List<dynamic>?;
    final photoReference = photos != null && photos.isNotEmpty
        ? photos[0]['photo_reference'] ?? ''
        : '';

    final adresse = json['formatted_address'] ?? '';
    final city = getCitySearch(adresse);

    return Place(
      id: json['place_id'] ?? '',
      name: json['name'] ?? '',
      imagePath: photoReference,
      adresse: adresse,
      rate: json['rating'] ?? 0.0,
      nbRate: json['user_ratings_total'] ?? 0.0,
      city: city,
    );
  }

  factory Place.fromJsonFromStorage(Map<String, dynamic> json) => Place(
      id: json['id'],
      name: json['name'],
      imagePath: json['imagePath'],
      adresse: json['adresse'],
      rate: json['rate'],
      nbRate: json['nbRate'],
      city: json['city']);

  Map<String, dynamic> toJson() => {'id': id, 'name': name, 'imagePath': imagePath, 'adresse': adresse, 'rate': rate, 'nbRate': nbRate, 'city': city};

  static String getCity(adresse) {
    if (adresse.isEmpty) {
      return '';
    }
    final city = adresse.trim().split(' ').last;
    return city;
  }

  static String getCitySearch(adresse) {
    if (adresse.isEmpty) {
      return '';
    }

    final words = adresse.trim().split(' ');

    final lastIndex = words.length - 2;
    final city = words[lastIndex].replaceAll(',', '');

    return city;
  }
}
