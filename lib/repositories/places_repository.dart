import '../models/place.dart';
import 'package:dio/dio.dart';

class PlacesRepository {
  final dio =
      Dio(BaseOptions(baseUrl: 'https://maps.googleapis.com/maps/api/place'));
  final apiKey = 'AIzaSyDWRTWkImZ1rC7AFiABDhjMqsY9twxWDPE';

  Future<List<Place>> fetchPlacesNearEiffelTower() async {
    final response = await dio.get('/nearbysearch/json', queryParameters: {
      'location': '48.858370,2.294481',
      'radius': '1500',
      'type': 'tourist_attraction',
      'language': 'fr',
      'key': apiKey
    });

    if (response.statusCode == 200) {
      final List<Place> places = [];

      final data = response.data as Map<String, dynamic>;
      final results = data['results'] as List<dynamic>;

      for (Map<String, dynamic> placeJson in results) {
        final place = Place.fromJson(placeJson);
        places.add(place);
      }
      return places;
    } else {
      throw Exception();
    }
  }

  Future<List<Place>> filterPlaces(List<Place> places, String query) async {
    if (query.length < 3 || query.isEmpty) {
      return places;
    }

    final response = await dio.get(
      '/textsearch/json',
      queryParameters: {
        'input': query,
        'inputtype': 'textquery',
        'type': 'tourist_attraction',
        'language': 'fr',
        'key': apiKey,
      },
    );

    if (response.statusCode == 200) {
      final filteredPlaces = <Place>[];

      final data = response.data as Map<String, dynamic>;
      final results = data['results'] as List<dynamic>;

      for (Map<String, dynamic> placeJson in results) {
        final place = Place.fromJsonSearch(placeJson);
        filteredPlaces.add(place);
      }

      return filteredPlaces;
    } else {
      throw Exception('Erreur lors de la recherche de lieux');
    }
  }

  Future<List<String>> getPhotoPlace(String placeId) async {
    final response = await dio.get('/details/json', queryParameters: {
      'fields': 'photos',
      'place_id': placeId,
      'language': 'fr',
      'key': apiKey,
    });

    if (response.statusCode == 200) {
      final data = response.data as Map<String, dynamic>;
      final photos = data['result']['photos'] as List<dynamic>;

      final photoReferences = photos.map((photo) {
        return photo['photo_reference'] as String;
      }).toList();

      return photoReferences;
    } else {
      throw Exception('Erreur lors de la recherche de lieux');
    }
  }

  Future<List<Place>> fetchPlacesNearUser(String latitude, String longitude) async {
    final response = await dio.get('/nearbysearch/json', queryParameters: {
      'location': '$latitude,$longitude',
      'rankby': 'distance',
      'type': 'tourist_attraction',
      'language': 'fr',
      'key': apiKey
    });

    if (response.statusCode == 200) {
      final List<Place> places = [];

      final data = response.data as Map<String, dynamic>;
      final results = data['results'] as List<dynamic>;

      for (Map<String, dynamic> placeJson in results) {
        final place = Place.fromJson(placeJson);
        places.add(place);
      }
      return places;
    } else {
      throw Exception();
    }
  }
}
