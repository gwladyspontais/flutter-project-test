import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:travel_book/models/my_place.dart';

class MyPlacesRepository {
  final storage = const FlutterSecureStorage();

  Future<List<MyPlace>> loadMyPlacesList() async {
    String? jsonList = await storage.read(key: "myPlaces");
    if (jsonList != null) {
      List<dynamic> decodedList = jsonDecode(jsonList);
      List<MyPlace> myPlacesList = decodedList.map((item) => MyPlace.fromJson(item)).toList();
      return myPlacesList;
    } else {
      return [];
    }
  }

  Future<void> saveMyPlaces(List<MyPlace> myPlacesList) async {
    String myPlacesListJson = jsonEncode(myPlacesList);
    await storage.write(key: "myPlaces", value: myPlacesListJson);
  }
}