import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../models/chapter.dart';

class ChaptersRepository {

  final storage = const FlutterSecureStorage();

  Future<List<Chapter>> getChapters() async {
    String? jsonList = await storage.read(key: "chapters");
    if (jsonList != null) {
      return jsonDecode(jsonList).toList();
    } else {
      return [];
    }
  }

  Future<void> setChapters(List<Chapter> chapters) async {
    final List<String> list = chapters.map((chapter) => chapter.toJson().toString()).toList();
    await storage.write(key: "chapters", value: jsonEncode(list));
  }
}