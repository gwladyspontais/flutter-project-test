import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:travel_book/models/profile.dart';

class UserRepository {
  UserRepository(this.firebaseAuth, this.firestore);

  final FirebaseAuth firebaseAuth;
  final FirebaseFirestore firestore;

  Future<bool> init() async {
    return firebaseAuth.currentUser != null;
  }

  Future<void> login(String email, String password) async {
    await firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<void> register(String email, String pseudo, String password) async {
    await firebaseAuth.createUserWithEmailAndPassword(email: email, password: password)
        .then((value) => {
          firestore.doc('users/${firebaseAuth.currentUser?.uid}')
            .set({'profile': Profile(pseudo, false).toJson()})
        }
    );
  }

  Future<void> resetPassword(String email) async {
    await firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<void> logout() async {
    await firebaseAuth.signOut();
  }

  Future<void> deleteUser() async {
    try {
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        await firestore
            .doc('users/${user.uid}')
            .delete()
            .then((value) => user.delete());
      } else {
        log('Aucun utilisateur connecté.');
      }
    } catch (e) {
      log('Erreur lors de la suppression du compte : $e');
    }
  }

  Future<Profile> getProfile() async {
    final doc = await firestore.doc('users/${firebaseAuth.currentUser?.uid}').get();
    final Map<String, dynamic> data = doc.data() ?? {};
    return Profile.fromJson(data['profile']);
  }

  Future<void> updateProfile(Profile profile) async {
    await firestore
        .doc('users/${firebaseAuth.currentUser?.uid}')
        .set({'profile': profile.toJson()});
  }

  Future<void> setLocalisationPermitted(bool localisationPermitted) async {
    try {
      await firestore.doc('users/${firebaseAuth.currentUser?.uid}').update({'profile.localisationPermitted': localisationPermitted});
    } catch (e) {
      log('Erreur lors de la mise à jour de la permission de localisation : $e');
    }
  }

  Future<void> setDarkMode(bool isDarkMode) async {
    try {
      await firestore.doc('users/${firebaseAuth.currentUser?.uid}').update({'profile.darkMode': isDarkMode});
    } catch (e) {
      log('Erreur lors de la mise à jour de la permission de localisation : $e');
    }
  }
}