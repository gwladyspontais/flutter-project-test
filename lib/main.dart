import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:travel_book/blocs/my_places_cubit.dart';
import 'package:travel_book/blocs/places_cubit.dart';
import 'package:travel_book/repositories/chapters_repository.dart';
import 'package:travel_book/repositories/my_places_repository.dart';
import 'package:travel_book/repositories/places_repository.dart';
import 'package:travel_book/repositories/user_repository.dart';
import 'package:travel_book/ui/pages/chapter_page.dart';
import 'package:travel_book/ui/pages/chapters_page.dart';
import 'package:travel_book/ui/pages/home_page.dart';
import 'package:travel_book/ui/pages/login_page.dart';
import 'package:travel_book/ui/pages/profile_page.dart';
import 'package:travel_book/ui/pages/search_page.dart';
import 'package:travel_book/ui/pages/travelogue_page.dart';
import 'package:travel_book/ui/themes/custom_theme.dart';

import 'blocs/chapters_cubit.dart';
import 'blocs/profile_cubit.dart';
import 'blocs/profile_state.dart';
import 'blocs/user_cubit.dart';
import 'blocs/user_state.dart';
import 'models/data_state.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Force l'écran en mode portrait
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await Firebase.initializeApp();
  final UserRepository userRepository =
      UserRepository(FirebaseAuth.instance, FirebaseFirestore.instance);
  final UserCubit userCubit = UserCubit(userRepository);
  await userCubit.init();

  final PlacesRepository placesRepository = PlacesRepository();
  final ProfileCubit profileCubit = ProfileCubit(userRepository);
  final PlacesCubit placesCubit = PlacesCubit(placesRepository, profileCubit);
  final MyPlacesRepository myPlacesRepository = MyPlacesRepository();
  final ChaptersRepository chaptersRepository = ChaptersRepository();

  runApp(MultiBlocProvider(providers: [
    BlocProvider(create: (_) => userCubit),
    BlocProvider(create: (_) => placesCubit),
    BlocProvider(create: (_) => ProfileCubit(userRepository)),
    BlocProvider(create: (_) => ChaptersCubit(chaptersRepository)),
    BlocProvider(create: (_) => MyPlacesCubit(myPlacesRepository)),
  ], child: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    final ProfileCubit profileCubit = context.read<ProfileCubit>();
    profileCubit.initProfile();

    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (context, state) {
        return KeyboardVisibilityProvider(
          child: MaterialApp(
            title: 'Travel Book',
            debugShowCheckedModeBanner: false,
            theme: state.profile?.darkMode == true ? CustomTheme.darkTheme : CustomTheme.lightTheme,
            routes: {
              '/home': (context) => const HomePage(),
              '/profile': (context) => const ProfilePage(),
              '/travelogue': (context) => const TraveloguePage(),
              '/search': (context) => const SearchPage(),
              '/chapter': (context) => const ChapterPage(),
              '/chapters': (context) => const ChaptersPage(),
            },
            home: BlocBuilder<UserCubit, UserState>(
                builder: (context, state) => state.status == AuthStatus.connected
                    ? const App()
                    : const LoginPage()),
          ),
        );
      }
    );
  }
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  int _selectedIndex = 0;

  static const List<Widget> pages = <Widget>[
    HomePage(),
    SearchPage(),
    TraveloguePage(),
    ChaptersPage(),
    ProfilePage()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        iconSize: 24,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Recherche',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu_book_rounded),
            label: 'Carnet',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book_outlined),
            label: 'Chapitres',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profil',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
