import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';

// Source : https://blog.logrocket.com/implementing-secure-password-reset-flutter-firebase/

enum AuthError {
  wrongPassword,
  emailAlreadyExists,
  invalidEmail,
  weakPassword,
  unknown,
}

class AuthExceptionHandler {
  static handleAuthException(FirebaseAuthException e) {
    log(e.code);
    AuthError error;
    switch (e.code) {
      case "invalid-email":
        error = AuthError.invalidEmail;
        break;
      case "wrong-password":
        error = AuthError.wrongPassword;
        break;
      case "weak-password":
        error = AuthError.weakPassword;
        break;
      case "email-already-in-use":
        error = AuthError.emailAlreadyExists;
        break;
      case "user-not-found":
        return null;
      default:
        error = AuthError.unknown;
    }
    return error;
  }
  static String generateErrorMessage(error) {
    String errorMessage;
    switch (error) {
      case AuthError.invalidEmail:
        errorMessage = "Le format de l'e-mail n'est pas valide.";
        break;
      case AuthError.weakPassword:
        errorMessage = "Le mot de passe doit avoir au moins 6 caractères.";
        break;
      case AuthError.wrongPassword:
        errorMessage = "Votre e-mail ou votre mot de passe est invalide.";
        break;
      case AuthError.emailAlreadyExists:
        errorMessage =
        "Cette adresse e-mail est déjà utilisée.";
        break;
      default:
        errorMessage = "Une erreur inconnue est survenue. Veuillez réessayer ou contacter le support.";
    }
    return errorMessage;
  }
}